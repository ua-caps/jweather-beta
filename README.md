# jweather-beta

## Prerequisites

### `Git`

Download and intall [git](https://git-scm.com/downloads).

### `node.js/npm`

Download and install [node.js/npm](https://nodejs.org/en/).

## Clone

```bash
# clone jweather-beta
$ git clone https://gitlab.com/ua-caps/jweather-beta.git
```

## Build

```bash
# cd into the project directory
$ cd jweather-beta

# install dependencies
$ npm install
```

## Run!

```bash
# serve with hot reload at localhost:43944
$ npm run dev
```

Open your favorite browser and visit [http://localhost:43944/](http://localhost:43944/)
